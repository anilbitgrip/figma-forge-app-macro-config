import ForgeUI, {
  render,
  Fragment,
  Macro,
  Text,
  Image,
  ConfigForm,
  TextField,
  useAction,
  useConfig
} from "@forge/ui";
import API from "@forge/api";

// Step1: Change with your Figma file id, you can find it on the URL
// Example URL: https://www.figma.com/file/v81VVvjuTkXpbn62PUi4wp/forge-app?node-id=0%3A1
const FIGMA_FILE_ID = "v81VVvjuTkXpbn62PUi4wp";

// https://www.figma.com/developers/api#get-images-endpoint
const FIGMA_GET_IMAGES_URL = "https://api.figma.com/v1/images/";

// Step2: Add Figma access token to Forge environments variables
// Where to find Figam access token? https://www.figma.com/developers/api#authentication
// How to add Forge environments variables? - https://developer.atlassian.com/platform/forge/environments/#environment-variables
const FIGMA_ACCESS_TOKEN = process.env.FORGE_USER_VAR_FIGMA_ACCESS_TOKEN;

const getFigmaElementImage = async elementID => {
  const element = await API.fetch(
    `${FIGMA_GET_IMAGES_URL}${FIGMA_FILE_ID}?ids=${elementID}`,
    {
      headers: {
        "X-Figma-Token": FIGMA_ACCESS_TOKEN
      }
    }
  ).then(response => {
    return response.json();
  });

  return {
    src: element.images[elementID],
    title: `image for figma element with id - ${elementID}`
  };
};

const App = () => {
  // Retrieve the configuration
  const config = useConfig();

  const [{ src, title }] = useAction(
    () => getFigmaElementImage(config.elementID),
    getFigmaElementImage(config.elementID)
  );

  // Use the configuration values
  return (
    <Fragment>
      <Text content={`Figma element ID is ${config.elementID}`} />
      <Image src={src} alt={title} />
    </Fragment>
  );
};

// Function that defines the configuration UI
const Config = () => {
  return (
    <ConfigForm>
      {/* Step3: Findout Figma nodeid to render on the Forge UI macro app using any REST client like  POSTMAN(https://www.postman.com/downloads/).
        Make sure to pass your figma access token  as X-Figma-Token in the header
        // Figma Nodes api - https://www.figma.com/developers/api#get-file-nodes-endpoint
        Example: https://api.figma.com/v1/files/v81VVvjuTkXpbn62PUi4wp/nodes?ids=5:2
      */}
      <TextField name="elementID" label="Figma element ID" />
    </ConfigForm>
  );
};

// A macro containing props for the app code, configuration,
// and default configuration values.
export const run = render(
  <Macro
    app={<App />}
    config={<Config />}
    defaultConfig={{
      // default Figma nodeid to render
      elementID: "5:2"
    }}
  />
);
